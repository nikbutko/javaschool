package com.tsystems.javaschool.tasks.calculator;

import java.text.DecimalFormat;
import java.util.*;

public class Calculator {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     * parentheses, operations signs '+', '-', '*', '/'<br>
     * Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */

    private final Map<String, Integer> operators = new HashMap<String, Integer>() {{
        put("+", 0);
        put("-", 0);
        put("*", 1);
        put("/", 1);
    }};

    private boolean isHigherPrecendence(String operator1, String operator2) {
        return operators.containsKey(operator2) &&
                operators.get(operator2) >= operators.get(operator1);
    }

    private String format(String number) {
        DecimalFormat df = new DecimalFormat("##.####");
        String result = df.format(Double.parseDouble(number));
        return result.replace(",", ".");
    }

    // Convert statement to Reverse Polish Notation via Shunting-yard algrorithm
    private List<String> convertToRPN(String infixStatement) {
        List<String> output = new LinkedList<>();
        Deque<String> stack = new LinkedList<>();
        String[] tokens = infixStatement.split("(?=[-+/*()])|(?<=[-+/*()])");

        for (String token : tokens) {
            if (operators.containsKey(token)) {
                while (!stack.isEmpty() && isHigherPrecendence(token, stack.peek()))
                    output.add(stack.pop());
                stack.push(token);
            } else if (token.equals("(")) {
                stack.push(token);
            } else if (token.equals(")")) {
                while (!stack.peek().equals("("))
                    output.add(stack.pop());
                stack.pop();
            } else {
                output.add(token);
            }
        }

        while (!stack.isEmpty())
            output.add(stack.pop());

        return output;
    }

    public String evaluate(String statement) {
        if (statement == null)
            return null;

        if (statement.contains("--") || statement.contains("++") || statement.contains("**")
                || statement.contains("//") || statement.isEmpty() || statement.contains(","))
            return null;

        List<String> rpnTokens = convertToRPN(statement);

        //If there is invalid count of parentheses in source statement,
        // then in the RPN-statement it will be only bracket – so its not right
        if (rpnTokens.contains("(") || rpnTokens.contains(")"))
            return null;

        Stack<String> stack = new Stack<String>();

        for (String token : rpnTokens) {
            if (!operators.containsKey(token))
                stack.push(token);
            else {
                double number1;
                double number2;

                try {
                    number1 = Double.parseDouble(stack.pop());
                    number2 = Double.parseDouble(stack.pop());
                } catch (NumberFormatException nfe) {
                    return null;
                }

                switch (token) {
                    case "+":
                        stack.push(String.valueOf(number1 + number2));
                        break;
                    case "-":
                        stack.push(String.valueOf(number2 - number1));
                        break;
                    case "*":
                        stack.push(String.valueOf(number1 * number2));
                        break;
                    case "/": {
                        if (number1 == 0.0)
                            return null;
                        stack.push(String.valueOf(number2 / number1));
                        break;
                    }
                }
            }
        }

        return format(stack.pop());
    }
}
